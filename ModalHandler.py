import json
import random

# Open the answers file, set x and y, and then change the
#       modals message text with the values and x and y
def pullQuestion():
        with open('answers.json', 'r') as ansFile:
                questions = json.load(ansFile)
        x = questions['first']
        y = questions['second']
        with open('modalMsg.json', 'r') as modalFile:
                JObject = json.load(modalFile)
        JObject['blocks'][0]['label']['text'] = "What is " + str(x) + " + " + str(y) + "?"
        return JObject

# Open the ansers file, get the JSON values, reset them, and then
#       write over the existing file with the new values
def initQuestion():
        with open('answers.json', 'r') as ansFile:
                questions = json.load(ansFile)
        x = random.randint(0, 100)
        y = random.randint(0, 100)
        questions['first'] = x
        questions['second'] = y
        questions['answer'] = x+y
        with open('answers.json', 'w+') as ansFile:
                json.dump(questions, ansFile)