from flask import Flask, render_template, request, Response
import os
import json
from slack import WebClient
from ModalHandler import pullQuestion, initQuestion
import requests

# Loads Flask App and sets the template folder.
app = Flask(__name__, template_folder='htmlDocs')

# Gets slack key from os, and creates client
SLACK_KEY = os.environ['SLACK_KEY']
client = WebClient(SLACK_KEY)

# Pull the json from the file and send the poll to the general chat
@app.route('/pull', methods=['POST'])
def SendModul():
        with open('initMsg.json') as msg:
                client.chat_postMessage(channel="general", blocks=json.load(msg))
        return Response(), 200

@app.route('/init', methods=['POST'])
def init():
        # Make sure the files exist
        if not os.path.exists('users.json'):
                with open('users.json', 'w') as usersFile:
                        pass
        # Setup the initial values which will be filled right after
        if not os.path.exists('answers.json'):
                with open('answers.json', 'w') as ansFile:
                        tmp_nums = ['first', 'second', 'answer']
                        tmp_dct = dict.fromkeys(tmp_nums, 0)
                        json.dump(tmp_dct, ansFile)
                        pass
        # Fill the answers.json with actual numbers instead of 0
        initQuestion()
        # Get all users, assign their usernames to a list, and create
        #       a dictionary with everyone set to 'NotAccounted'
        SlackInquiry = client.users_list()
        users = SlackInquiry['members']
        user_names = list(map(lambda u: u['name'], users))
        default_dict = dict.fromkeys(user_names, 'NotAccounted')
        # Use json to dump the information to a file
        with open('users.json', 'w+') as usersFile:
                json.dump(default_dict, usersFile)
        return Response(), 200

@app.route('/check', methods=['POST'])
def testfunction():
        # Create empty arrays and fill them in with all the usernames
        #       of everyone according to their status
        with open('users.json', 'r') as usersFile:
                json_names = json.load(usersFile)
        pdy = []
        tdyo = []
        tdys = []
        olv = []
        notA = []
        for (k, v) in json_names.items():
                if v == 'pdy':
                        pdy.append(k)
                elif v == 'tdy-o':
                        tdyo.append(k)
                elif v == 'tdy-s':
                        tdys.append(k)
                elif v == 'olv':
                        olv.append(k)
                else:
                        notA.append(k)
        # Check each array to see if they are not empty. If they are not
        #       then send a message with the arrays len and names with \n
        if pdy:
                postMessage('#00FF00', 'On PDY: ' + str(len(pdy)), '\n'.join(pdy))
        if tdyo:
                postMessage('#0000FF', 'On TDY-O: ' + str(len(tdyo)), '\n'.join(tdyo))
        if tdys:
                postMessage('#0000FF', 'On TDY-s: ' + str(len(tdys)), '\n'.join(tdys))
        if olv:
                postMessage('#FF0000', 'On OLV: ' + str(len(olv)), '\n'.join(olv))
        if notA:
                postMessage('#000000', 'Has not checked in: ' + str(len(notA)), '\n'.join(notA))
        return Response(), 200

# The initial entry for the users. Will check what type of payload they have
#       and perform the correct action
@app.route('/slack', methods=['POST'])
def inbound():
        JTest = json.loads(request.form.get('payload'))
        if JTest['type'] == 'view_submission':
                return userResponse(JTest)
        elif JTest['type'] == 'block_actions':
                postModal(JTest['trigger_id'])
                return Response(), 200
        else:
                return Response(), 400

def userResponse(json_req):
        name = json_req['user']['username']
        value = json_req['view']['state']['values']
        # The values are weird here. The response you get is very messy to read and hard to traverse.
        #       Slack sends it as {'{3-5 random chars}' {'{3-5 random chars}'} } for EACH block added
        #       So, we have to have a for loop to get past the first random chars followed by another
        #       for to get past the second. Then we need to determine if it is text input or drop-down.
        #       Text input is easy, but the drop-down has to go a bit further to get it's value
        for x in value:
                for y in value[x]:
                        if value[x][y]['type'] == 'plain_text_input':
                                text_input = value[x][y]['value']
                        elif value[x][y]['type'] == 'static_select':
                                select_input = value[x][y]['selected_option']['value']
        with open('answers.json', 'r') as ansFile:
                answers = json.load(ansFile)
        if text_input == str(answers['answer']):
                changeUser(name, select_input)
                return Response(), 200
        else:
                return Response(), 400

# Opens the files, gets the json list, changes the correct users values, and re-writes the file
def changeUser(name, value):
        with open('users.json', 'r') as usersFile:
                json_names = json.load(usersFile)
        json_names[name] = value
        with open('users.json', 'w+') as usersFile:
                json.dump(json_names, usersFile)

# Quick and dirty API to print to a specific users DM with color, header, and string
def postMessage(color, pre_string, string):
        client.chat_postMessage(channel='U01ALH29M16',
                attachments=[{
                        'color': color,
                        'blocks': [{
                                'type': 'section',
                                'text': {
                                        'type': 'mrkdwn',
                                        'text': pre_string + '\n' + string
                                }
                        }]
                }])

# Opens the Dynamic Modal from ModalHandler.py
def postModal(trigger):
        json_string = pullQuestion()
        client.views_open(trigger_id=trigger, view=json_string)

# Might do something with this... Maybe
if __name__ == "__main__":
        pass